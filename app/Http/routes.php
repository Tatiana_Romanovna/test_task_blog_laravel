<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','RecordsController@index');
Route::get('/admin','RecordsController@addRecord');
Route::post('/new_record','RecordsController@create');
Route::get('/blog/{name}','RecordsController@getOneRecord');
