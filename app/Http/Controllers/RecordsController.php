<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class RecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_records = Records::all();
        return view('records', ['all_records' => $all_records]);
    }

    public function resize($file, $type = 1, $rotate = null, $quality = null)
    {
        global $tmp_path;

        $max_thumb_size = 100;
        $max_size = 100;

        if ($quality == null)
            $quality = 75;

        if ($file['type'] == 'image/jpeg')
            $source = imagecreatefromjpeg($file['tmp_name']);
        elseif ($file['type'] == 'image/png')
            $source = imagecreatefrompng($file['tmp_name']);
        elseif ($file['type'] == 'image/gif')
            $source = imagecreatefromgif($file['tmp_name']);
        else
            return false;

        if ($rotate != null)
            $src = imagerotate($source, $rotate, 0);
        else
            $src = $source;

        $w_src = imagesx($src);
        $h_src = imagesy($src);

        if ($type == 1)
            $w = $max_thumb_size;
        elseif ($type == 2)
            $w = $max_size;

        if ($w_src > $w) {
            $ratio = $w_src / $w;
            $w_dest = round($w_src / $ratio);
            $h_dest = round($h_src / $ratio);

            if ($h_dest > $max_size) {
                $ratio = $h_dest / $max_size;
                $w_dest = round($w_dest / $ratio);
                $h_dest = round($h_dest / $ratio);
            }

            $dest = imagecreatetruecolor($w_dest, $h_dest);

            imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);

            imagejpeg($dest, $tmp_path . $file['name'], $quality);
            imagedestroy($dest);
            imagedestroy($src);

            return $file['name'];
        } else {
            imagejpeg($src, public_path() . '/' . $file['name'], $quality);
            imagedestroy($src);
            return $file['name'];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $image = $name = $this->resize($_FILES['img_preview']);

        $record = new Records();

        $record->setAttribute('created_date', date('Y-m-d'));
        $record->setAttribute('name', $request->input('title'));
        $record->setAttribute('preview', $request->input('text_preview'));
        $record->setAttribute('text', $request->input('big_text'));
        $record->setAttribute('photo', $image);
        $record->save();

        return Redirect::back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getOneRecord($name = '')
    {
        $one_record = Records::where('name', '=', $name)->first();
        return view('one_record', ['one_record' => $one_record]);
    }


}
