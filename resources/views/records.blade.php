@extends('main')

@section('content')

    <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
            <th>Photo</th>
            <th>Name</th>
            <th>Preview</th>
            <th class="text-right">Date</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($all_records as $record)
            <tr>
                <td>
                    <a href="/blog/{{$record->name}}">
                        <div style="background:url({{$record->photo}}) no-repeat 50% 50%; height: 100px; width: 100px;"></div>
                    </a>
                </td>
                <td>{{$record->name}}</td>
                <td>{{$record->preview}}</td>
                <td class="text-right">{{$record->created_date}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
