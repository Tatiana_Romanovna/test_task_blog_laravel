@extends('main')

@section('content')

    <div class="row">
        <div class=" col-md-12 text-center">
            <a href="/" class="btn btn-info">All records</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>{{$one_record->name}}</h3></div>
                <div class="panel-body">
                    <p>{{$one_record->text}}</p>

                    <p class="text-right">{{$one_record->created_date}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection