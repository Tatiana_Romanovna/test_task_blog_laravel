@extends('main')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading"><h3>Add new record</h3></div>
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="/new_record" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="form-group">
                    <label class="control-label col-sm-2" for="img_preview">Image preview:</label>

                    <div class="col-sm-10">
                        <input type="file" name="img_preview" class="form-control" id="img_preview">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="text_preview">Text preview:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="text_preview" id="text_preview"
                               placeholder="Text preview">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Name:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="big_text">Text:</label>

                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" name="big_text" id="big_text"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection